package ru.tersoft.hello.client;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;
import ru.tersoft.hello.Greeting;

/**
 * @author ivyanni
 * @version 1.0
 * Consumer of Hello World service for Apache Felix.
 * Calls method on start which prints "Hello world".
 */
public class Activator implements BundleActivator {
    public void start(BundleContext bundleContext) throws Exception {
        ServiceReference reference =
                bundleContext.getServiceReference(Greeting.class.getName());
        ((Greeting) bundleContext.getService(reference)).sayHello();
    }

    public void stop(BundleContext bundleContext) throws Exception {

    }
}
