package ru.tersoft.hello.impl;

import ru.tersoft.hello.Greeting;

/**
 * @author ivyanni
 * @version 1.0
 * Implementation of service which prints "hello world" message.
 */
public class GreetingImpl implements Greeting {
    public void sayHello() {
        System.out.println("Hello OSGi World!");
    }
}
