package ru.tersoft.hello.impl;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import ru.tersoft.hello.Greeting;

/**
 * @author ivyanni
 * @version 1.0
 * OSGi service activator. Registers service on bundle start.
 */
public class Activator implements BundleActivator {
    public void start(BundleContext bundleContext) throws Exception {
        bundleContext.registerService(Greeting.class.getName(),
                new GreetingImpl(), null);
    }

    public void stop(BundleContext bundleContext) throws Exception {

    }
}
